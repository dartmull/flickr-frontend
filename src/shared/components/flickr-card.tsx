import * as React from 'react';

import {Card, CardMedia, CardText, CardTitle} from 'react-toolbox';

import * as styles from './flickr-card.module.scss';

export default class FlickrCard extends React.Component<FlickrCardProps, {}> {
  public render() {
    return (
        <Card shadow={0} className={styles.card}>
          <CardMedia className={styles.imageContainer}>
            <img src={this.props.imageSource} alt={this.props.imageAlt} className={styles.image}/>
          </CardMedia>
          <CardTitle>{this.props.title}</CardTitle>
          <CardText className={styles.descriptionContainer}>{this.props.description}</CardText>
          <CardText className={styles.tagContainer}>
            <p>Tags: </p>
            {this.props.tags}
          </CardText>
        </Card>
    );
  }
}

interface FlickrCardProps {
  imageSource: string;
  imageAlt: string;
  title: JSX.Element;
  description: React.ReactNode;
  tags: JSX.Element;
}
const FRONTEND_URL = ENV_CONFIG.frontendUrl;

const config = {
    flickrUrl: 'https://api.flickr.com/services/feeds/photos_public.gne?format=json',
    contextPath: FRONTEND_URL,
    getAbsoluteContextPath(): string {
        return `/${this.contextPath.replace(/^\/+/, '')}`;
    }
};

export default config;
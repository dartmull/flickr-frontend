class ArrayUtils {
  public chunkArray<T>(array: T[], size: number): any[] {
    const chunkedArray: any[] = [];

    for (const i = 0; i < array.length; i + 1) {
      const last = chunkedArray[chunkedArray.length - 1];

      if (!last || last.length === size) {
        chunkedArray.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }

    return chunkedArray;
  }
}

export default new ArrayUtils();
import Axios from 'axios';

import FlickrResponse from './flickr-response';
import confing from './config';

class FlickrServices {
    public getPhotos(): Promise<FlickrResponse[]> {
        return Axios.get(confing.flickrUrl).then((response) => {
            console.log(response);

            return response.data;
        });
    }

}

export default new FlickrServices();
import * as React from 'react';
import { withRouter } from 'react-router';
import { Row, Col } from 'react-flexbox-grid';
import JsxParser from 'react-jsx-parser'

import FlickrResponse from '../shared/flickr-response';
import FlickrCard from '../shared/components/flickr-card';
import flickrService from '../shared/flickr-services';
import arrayUtils from '../shared/array-utils';

import * as styles from './flickr-list.module.scss';

class FlickrList extends React.Component<{}, FlickrListState> {
  private generateFlickrCard(flickrResponse: FlickrResponse): JSX.Element {
    const imageSource = flickrResponse.media.m;
    const imageAlt = flickrResponse.title;
    const author = flickrResponse.author.match(/\(([^)]+)\)/)![1];
    const authorLink = `'https://www.flickr.com/photos/${flickrResponse.author_id}`;
    const title = (
        <div className={styles.title}>
          <a href={imageSource}>{imageAlt}</a>
          <p> by </p>
          <a href={authorLink}>{author}</a>
        </div>
    );
    const tags = <JsxParser jsx={flickrResponse.tags}/>;
    const description = new DOMParser().parseFromString(flickrResponse.description, 'text/xml');
      return(
          <Col xs={12} md={6} lg={3}>
            <FlickrCard imageSource={imageSource}
                        imageAlt={imageAlt}
                        title={title}
                        description={description.firstChild}
                        tags={tags}/>
          </Col>
      );
  }

  private generateFlickrRow(flickrResponse: FlickrResponse[]): JSX.Element[] {
    const flickrCards: JSX.Element[] = [];
    const flickrRows = arrayUtils.chunkArray<FlickrResponse>(flickrResponse, 4);

    for (const i = 1; i <= flickrRows.length; i + 1) {
      const flickrRow = flickrRows[i];
      const flckrCols: JSX.Element[] = [];

      for (const i = 1; i <= flickrRow.length; i + 1) {
        flckrCols.push(this.generateFlickrCard(flickrRow[i]));
      }

      flickrCards.push(<Row>{flckrCols}</Row>);
    }

    return flickrCards;
  }

  public state: FlickrListState = {
    flickrResponse: [],
  };

  public componentWillMount() {
    flickrService.getPhotos()
        .then((flickrResponse: FlickrResponse[]) => {
          this.setState({ flickrResponse });
        });
  }

  public render() {
    const flickrResponse: FlickrResponse[] = this.state.flickrResponse;

    return this.state.flickrResponse.length > 0
        ? <div className={styles.listContainer}>{this.generateFlickrRow(flickrResponse)}</div>
        : null;
  }
}

interface FlickrListState {
  flickrResponse: FlickrResponse[];
}

export default withRouter(FlickrList);
import * as React from 'react';
import { Grid } from 'react-flexbox-grid';
import { withRouter } from 'react-router';

class App extends React.Component<{}, {}> {
    render() {
        return (
            <Grid>{this.props.children}</Grid>
        );
    }
}

export default withRouter(App);
import 'es6-shim';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, Route } from 'react-router';

import App from './app';
import FlickrList from '../flickr-list/flickr-list';
import routingHistory from '../shared/routing-history';

ReactDOM.render(
    <Router history={routingHistory}>
        <Route path={'/'} component={App}>
            <Route path={'flickr-list'} component={FlickrList}/>
        </Route>
    </Router>,
    document.getElementById('app'),
);

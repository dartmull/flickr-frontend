module.exports = {
  globals: {
    ENV_CONFIG: require('./webpack/env-config/unit-test-env-config.js')
  },
  preset: 'ts-jest',
  coverageReporters: ['json', 'text'],
  coverageDirectory: '<rootDir>/__coverage__/',
  verbose: true,
  setupTestFrameworkScriptFile: '<rootDir>/__test__/setup/setup-test-framework.ts',
  testEnvironment: 'enzyme',
  testMatch: ['**/__tests__/**/*.test.ts?(x)', '**/?(*.)+(test).ts?(x)'],
  testEnvironmentOptions: {
    enzymeAdapter: 'react15'
  },
  testPathIgnorePatterns: ['<rootDir>/__test__/setup/', '../../node_modules', 'build', 'tsbuild'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  moduleNameMapper: {
    '^.+\\.(css|less|scss)$': 'identity-obj-proxy',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/file-mock.ts'
  },
  moduleDirectories: ['node_modules']
};

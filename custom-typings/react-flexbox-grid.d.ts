declare module 'react-flexbox-grid' {
    import React = require('react');

    interface ColProps {
        xs?: number | boolean,
        sm?: number | boolean,
        md?: number | boolean,
        lg?: number | boolean,

        xsOffset?: number,
        smOffset?: number,
        mdOffset?: number,
        lgOffset?: number,

        className?: string,
    }
    class Col extends React.Component<ColProps, {}> {
    }

    interface RowProps {
        start?: number,
        center?: string,
        end?: string,

        top?: string,
        middle?: string,
        bottom?: string,

        around?: string,
        between?: string,
        reverse?: boolean,

        first?: string,
        last?: string,

        className?: string,
        tagName?: string,
    }
    class Row extends React.Component<RowProps, {}> {
    }

    interface GridProps {
        fluid?: boolean,
        className?: string,
        tagName?: string,
    }
    class Grid extends React.Component<GridProps, {}> {
    }
}
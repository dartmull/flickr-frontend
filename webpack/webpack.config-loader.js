const path = require("path");
const sanitize = require("sanitize-filename");

const environment = process.env.APPLICATION_ENVIRONMENT || "dev";
const profileConfig = require(path.join(__dirname, sanitize("webpack-" + environment + ".config")));

module.exports = profileConfig;
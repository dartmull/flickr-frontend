const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

function createConfig(options) {
  options = options || {};

  const ENV_CONFIG = require('./env-config/' + options.envConfigFile);

  for (const property in ENV_CONFIG) {
    if (typeof ENV_CONFIG[property] === 'string') {
      ENV_CONFIG[property] = JSON.stringify(ENV_CONFIG[property]);
    }
  }

  const contextPath = options.contextPath || '/flickr-frontend/';

  const tsPreLoaders = [
    {
      loader: 'babel-loader',
      options: {
        presets: ['babel-preset-es2015'].map(require.resolve)
      }
    }
  ];

  if (options.enableReactHotLoader) {
    tsPreLoaders.unshift('react-hot-loader/webpack');
  }

  const tsLoader = 'ts-loader' + (options.ignoreTsErrors ? '?transpileOnly=true' : '');

  // we set these variables for the cyclic dependencies plugin
  // MAX_CYCLES is the current number of cyclic dependencies that we have right now
  // we don't want to create any new cyclic dependencies as this may result in a React/TypeScript failure
  const MAX_CYCLES = 9;
  let numCyclesDetected = 0;

  const config = {
    entry: {
      // We need babel-polyfill in order to use the async and await keywords. Without it we get a runtime error.
      main: ['babel-polyfill', './src/app/index.tsx']
    },
    module: {
      rules: [
        {
          test: /webpack-dev-server/,
          use: 'babel-loader?presets[]=es2015'
        },
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: tsPreLoaders.concat(tsLoader)
        },
        {
          test: /\.bundle\.tsx?$/,
          exclude: /node_modules/,
          use: [].concat('bundle-loader?lazy=true', tsPreLoaders, tsLoader)
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader', 'postcss-loader'],
          exclude: /flexboxgrid|react-toolbox/
        },
        // Configuration for react-flexbox-grid and react-toolbox
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
            'postcss-loader?sourceMap&sourceComments'
          ],
          include: /flexboxgrid|react-toolbox/
        },
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
          exclude: /react-toolbox|\.module\.scss$/
        },
        {
          test: /\.scss$/,
          use: [
            'style-loader',
            'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
            'postcss-loader?sourceMap&sourceComments',
            'sass-loader'
          ],
          include: /react-toolbox|\.module\.scss$/
        },
        // Needed for loading bootstrap
        { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: 'file-loader' },
        // Load all fonts into the dist directory. This is needed for the bootstrap because it uses
        // them. Next regex will cause all of the files with specified extension and version(if
        // exists) to be added.
        { test: /\.woff(2?)(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?prefix=font/&limit=5000' },
        {
          test: /\.(ttf|gif|png|jpg)(\?v=\d+\.\d+\.\d+)?$/,
          use: 'url-loader?limit=1000&name=[name].[hash].[ext]',
          exclude: /\.no-sprite\.(ttf|gif|png|jpg)/
        },
        {
          test: /\.no-sprite.(ttf|gif|png|jpg|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: 'file-loader?name=[name].[hash].[ext]'
        },
        {
          test: /\.svg$/,
          exclude: /\.no-sprite\.svg/,
          loader: 'svg-sprite-loader',
          options: {
            symbolId: '[name]_[hash]',
            runtimeCompat: true // TODO: (Iliya) update usages and remove this option
          }
        },
        // Needed for loading html files (like index.html). Provides JS string interpolation
        // capabilities, so that the html files can contain JS code
        {
          test: /\.html$/,
          use: 'html-loader?interpolate'
        },
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          enforce: 'pre',
          use: [
            {
              loader: 'tslint-loader',
              options: {
                // tslint errors are displayed by default as warnings
                // set emitErrors to true to display them as errors
                emitErrors: true,

                // tslint does not interrupt the compilation by default
                // if you want any file with tslint errors to fail
                // set failOnHint to true
                failOnHint: true
              }
            }
          ]
        },
        // Needed for loading shaders directly in the DOM (e.g. heatmap.fragment.fx)
        {
          test: /\.fx$/,
          use: 'raw-loader'
        }
      ]
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.scss', '.css']
    },
    output: {
      path: __dirname + '/../dist',
      publicPath: contextPath,
      filename: '[name].[hash].js',
      chunkFilename: '[name].bundle.[chunkhash].js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        // Use the html-loader, so we can interpolate the content with some JS
        template: 'src/app/index.html'
      }),
      new webpack.LoaderOptionsPlugin({
        debug: true,
        minimize: !!options.shouldUglifyScripts
      }),
      new webpack.DefinePlugin({
        ENV_CONFIG: ENV_CONFIG
      }),
      new CircularDependencyPlugin({
        onStart({ compilation }) {
          numCyclesDetected = 0;
        },
        onDetected({ module: webpackModuleRecord, paths, compilation }) {
          numCyclesDetected++;
          compilation.warnings.push(new Error(paths.join(' -> ')));
        },
        onEnd({ compilation }) {
          if (numCyclesDetected > MAX_CYCLES) {
            compilation.errors.push(
              new Error(
                `Detected ${numCyclesDetected} cycles which exceeds configured limit of ${MAX_CYCLES}`
              )
            );
          }
        }
      })
    ],
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/](?!webpack)/,
            chunks: 'initial',
            name: 'vendor',
            filename: '[name].[chunkhash].js',
            priority: 10,
            enforce: true
          }
        }
      },
      minimize: options.shouldUglifyScripts,
      minimizer: [new UglifyJsPlugin({ sourceMap: !!options.shouldGenerateSourceMaps })]
    }
  };

  if (options.shouldGenerateSourceMaps) {
    config.devtool = 'source-map';
  }

  if (options.enableReactHotLoader) {
    config.entry.main.unshift(
      'webpack-dev-server/client?https://localhost:9443',
      'webpack/hot/only-dev-server'
    );
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
  }

  config.mode = options.mode || 'production';

  return config;
}

module.exports = createConfig;

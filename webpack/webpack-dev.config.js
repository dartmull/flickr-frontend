const createConfig = require('./create-config');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = Object.assign(
  createConfig({
    envConfigFile: 'dev-env-config.js',
    shouldGenerateSourceMaps: true,
    enableReactHotLoader: true,
    mode: 'development'
  }),
  {
    devServer: {
      port: 9443,
      hot: true,
      https: true,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
      },
      historyApiFallback: {
        index: '/wsd-frontend/index.html'
      }
    }
  }
);

// Adding [chunkhash] to the name does not work with hot reloading, because it may lead to
// excessive memory usage. That's why we have to strip it from the output filename.
config.output.filename = '[name].bundle.js';

if (process.env.ENABLE_ANALYZER) {
  config.plugins.push(
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      reportFilename: 'main-bundles-report.html'
    })
  );
}

module.exports = config;

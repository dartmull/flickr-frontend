#INSTALLATION, BUILD AND RUN STEPS

1. Download and install NodeJS v8.11.3

2. Make sure your npm version is v5.6.0

3. Download and install GitBash

4. Go with GitBash to the directory where you checkout the project.

5. Run the following commands to start the server: 
   npm install
   npm run start


